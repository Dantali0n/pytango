---
Contributers: PyTango Team - see [source repo history](https://gitlab.com/tango-controls/pytango/-/graphs/develop?ref_type=heads)
  for full details.
Last Update: '{{ today }}'
---

(pytango-history-changes)=

# History of changes

(pytango-revisions)=

## Document revisions

| Date     | Revision                                                                       | Description                                                             | Author                      |
|----------|--------------------------------------------------------------------------------|-------------------------------------------------------------------------|-----------------------------|
| 18/07/03 | 1.0                                                                            | Initial Version                                                         | M. Ounsy                    |
| 06/10/03 | 2.0                                                                            | Extension of the "Getting Started" paragraph                            | A. Buteau/M. Ounsy          |
| 14/10/03 | 3.0                                                                            | Added Exception Handling paragraph                                      | M. Ounsy                    |
| 13/06/05 | 4.0                                                                            | Ported to Latex, added events, AttributeProxy and ApiUtil               | V. Forchì                   |
| 13/06/05 | 4.1                                                                            | fixed bug with python 2.5 and and state events new Database constructor | V. Forchì                   |
| 15/01/06 | 5.0                                                                            | Added Device Server classes                                             | E.Taurel                    |
| 15/03/07 | 6.0                                                                            | Added AttrInfoEx, AttributeConfig events, 64bits, write_attribute       | T. Coutinho                 |
| 21/03/07 | 6.1                                                                            | Added groups                                                            | T. Coutinho                 |
| 15/06/07 | [6.2](http://www.tango-controls.org/Documents/bindings/PyTango-3.0.3.pdf)      | Added dynamic attributes doc                                            | E. Taurel                   |
| 06/05/08 | [7.0](http://www.tango-controls.org/Documents/bindings/PyTango-3.0.4.pdf)      | Update to Tango 6.1. Added DB methods, version info                     | T. Coutinho                 |
| 10/07/09 | [8.0](http://www.tango-controls.org/static/PyTango/v7/doc/html/index.html)     | Update to Tango 7. Major refactoring. Migrated doc                      | T. Coutinho/R. Suñe         |
| 24/07/09 | [8.1](http://www.tango-controls.org/static/PyTango/v7/doc/html/index.html)     | Added migration info, added missing API doc                             | T. Coutinho/R. Suñe         |
| 21/09/09 | [8.2](http://www.tango-controls.org/static/PyTango/v7/doc/html/index.html)     | Added migration info, release of 7.0.0beta2                             | T. Coutinho/R. Suñe         |
| 12/11/09 | [8.3](http://www.tango-controls.org/static/PyTango/v71/doc/html/index.html)    | Update to Tango 7.1.                                                    | T. Coutinho/R. Suñe         |
| ??/12/09 | [8.4](http://www.tango-controls.org/static/PyTango/v71rc1/doc/html/index.html) | Update to PyTango 7.1.0 rc1                                             | T. Coutinho/R. Suñe         |
| 19/02/10 | [8.5](http://www.tango-controls.org/static/PyTango/v711/doc/html/index.html)   | Update to PyTango 7.1.1                                                 | T. Coutinho/R. Suñe         |
| 06/08/10 | [8.6](http://www.tango-controls.org/static/PyTango/v712/doc/html/index.html)   | Update to PyTango 7.1.2                                                 | T. Coutinho                 |
| 05/11/10 | [8.7](http://www.tango-controls.org/static/PyTango/v713/doc/html/index.html)   | Update to PyTango 7.1.3                                                 | T. Coutinho                 |
| 08/04/11 | [8.8](http://www.tango-controls.org/static/PyTango/v714/doc/html/index.html)   | Update to PyTango 7.1.4                                                 | T. Coutinho                 |
| 13/04/11 | [8.9](http://www.tango-controls.org/static/PyTango/v715/doc/html/index.html)   | Update to PyTango 7.1.5                                                 | T. Coutinho                 |
| 14/04/11 | [8.10](http://www.tango-controls.org/static/PyTango/v716/doc/html/index.html)  | Update to PyTango 7.1.6                                                 | T. Coutinho                 |
| 15/04/11 | [8.11](http://www.tango-controls.org/static/PyTango/v720/doc/html/index.html)  | Update to PyTango 7.2.0                                                 | T. Coutinho                 |
| 12/12/11 | [8.12](http://www.tango-controls.org/static/PyTango/v722/doc/html/index.html)  | Update to PyTango 7.2.2                                                 | T. Coutinho                 |
| 24/04/12 | [8.13](http://www.tango-controls.org/static/PyTango/v723/doc/html/index.html)  | Update to PyTango 7.2.3                                                 | T. Coutinho                 |
| 21/09/12 | [8.14](http://www.tango-controls.org/static/PyTango/v800/doc/html/index.html)  | Update to PyTango 8.0.0                                                 | T. Coutinho                 |
| 10/10/12 | [8.15](http://www.tango-controls.org/static/PyTango/v802/doc/html/index.html)  | Update to PyTango 8.0.2                                                 | T. Coutinho                 |
| 20/05/13 | [8.16](http://www.tango-controls.org/static/PyTango/v803/doc/html/index.html)  | Update to PyTango 8.0.3                                                 | T. Coutinho                 |
| 28/08/13 | [8.13](http://www.tango-controls.org/static/PyTango/v723/doc/html/index.html)  | Update to PyTango 7.2.4                                                 | T. Coutinho                 |
| 27/11/13 | [8.18](http://www.tango-controls.org/static/PyTango/v811/doc/html/index.html)  | Update to PyTango 8.1.1                                                 | T. Coutinho                 |
| 16/05/14 | [8.19](http://www.tango-controls.org/static/PyTango/v812/doc/html/index.html)  | Update to PyTango 8.1.2                                                 | T. Coutinho                 |
| 30/09/14 | [8.20](http://www.tango-controls.org/static/PyTango/v814/doc/html/index.html)  | Update to PyTango 8.1.4                                                 | T. Coutinho                 |
| 01/10/14 | [8.21](http://www.tango-controls.org/static/PyTango/v815/doc/html/index.html)  | Update to PyTango 8.1.5                                                 | T. Coutinho                 |
| 05/02/15 | [8.22](http://www.esrf.fr/computing/cs/tango/pytango/v816/index.html)          | Update to PyTango 8.1.6                                                 | T. Coutinho                 |
| 03/02/16 | [8.23](http://www.esrf.fr/computing/cs/tango/pytango/v818/index.html)          | Update to PyTango 8.1.8                                                 | T. Coutinho                 |
| 12/08/16 | 8.24                                                                           | Update to PyTango 8.1.9                                                 | V. Michel                   |
| 26/02/16 | [9.2.0a](http://www.esrf.fr/computing/cs/tango/pytango/v920)                   | Update to PyTango 9.2.0a                                                | T. Coutinho                 |
| 15/08/16 | [9.2.0](http://pytango.readthedocs.io/en/v9.2.0)                               | 9.2.0 Release                                                           | V. Michel                   |
| 23/01/17 | [9.2.1](http://pytango.readthedocs.io/en/v9.2.1)                               | 9.2.1 Release                                                           | V. Michel                   |
| 27/09/17 | [9.2.2](http://pytango.readthedocs.io/en/v9.2.2)                               | 9.2.2 Release                                                           | G. Cuni/V. Michel/J. Moldes |
| 30/05/18 | [9.2.3](http://pytango.readthedocs.io/en/v9.2.3)                               | 9.2.3 Release                                                           | V. Michel                   |
| 30/07/18 | [9.2.4](http://pytango.readthedocs.io/en/v9.2.4)                               | 9.2.4 Release                                                           | V. Michel                   |
| 28/11/18 | [9.2.5](http://pytango.readthedocs.io/en/v9.2.5)                               | 9.2.5 Release                                                           | A. Joubert                  |
| 13/03/19 | [9.3.0](http://pytango.readthedocs.io/en/v9.3.0)                               | 9.3.0 Release                                                           | T. Coutinho                 |
| 08/08/19 | [9.3.1](http://pytango.readthedocs.io/en/v9.3.1)                               | 9.3.1 Release                                                           | A. Joubert                  |
| 30/04/20 | [9.3.2](http://pytango.readthedocs.io/en/v9.3.2)                               | 9.3.2 Release                                                           | A. Joubert                  |
| 24/12/20 | [9.3.3](http://pytango.readthedocs.io/en/v9.3.3)                               | 9.3.3 Release                                                           | A. Joubert                  |
| 14/06/22 | [9.3.4](http://pytango.readthedocs.io/en/v9.3.4)                               | 9.3.4 Release                                                           | A. Joubert                  |
| 07/09/22 | [9.3.5](http://pytango.readthedocs.io/en/v9.3.5)                               | 9.3.5 Release                                                           | Y. Matveev                  |
| 28/09/22 | [9.3.6](http://pytango.readthedocs.io/en/v9.3.6)                               | 9.3.6 Release                                                           | Y. Matveev                  |
| 15/02/23 | [9.4.0](http://pytango.readthedocs.io/en/v9.4.0)                               | 9.4.0 Release                                                           | A. Joubert                  |
| 15/03/23 | [9.4.1](http://pytango.readthedocs.io/en/v9.4.1)                               | 9.4.1 Release                                                           | A. Joubert                  |
| 27/07/23 | [9.4.2](http://pytango.readthedocs.io/en/v9.4.2)                               | 9.4.2 Release                                                           | Y. Matveev                  |
| 23/11/23 | [9.5.0](http://pytango.readthedocs.io/en/v9.5.0)                               | 9.5.0 Release                                                           | A. Joubert                  |
| 28/03/24 | [9.5.1](http://pytango.readthedocs.io/en/v9.5.1)                               | 9.5.1 Release                                                           | A. Joubert                  |
| 01/10/24 | [10.0.0](http://pytango.readthedocs.io/en/v10.0.0)                             | 10.0.0 Release                                                          | A. Joubert                  |

(pytango-version-history)=

## Version history

### 10.0.0

#### Changed

- [!646: New Asyncio servers implementation](https://gitlab.com/tango-controls/pytango/-/merge_requests/646)
- [!648: PyTango switched to require C++17 when building](https://gitlab.com/tango-controls/pytango/-/merge_requests/648)
- [!654: Redirect server run errors to stderr instead of stdout](https://gitlab.com/tango-controls/pytango/-/merge_requests/654)
- [!659: Improve command arg type errors](https://gitlab.com/tango-controls/pytango/-/merge_requests/659)
- [!663: Switch to cppTango 10.0.0 and add Device_6Impl](https://gitlab.com/tango-controls/pytango/-/merge_requests/663)
- [!664: Use attribute lock ATTR_BY_KERNEL for push_event](https://gitlab.com/tango-controls/pytango/-/merge_requests/664)
- [!631: Improve error message if cannot convert value to DevBoolean type](https://gitlab.com/tango-controls/pytango/-/merge_requests/631)
- [!673: Improve error message if python float value if written to int attribute for python >= 3.10](https://gitlab.com/tango-controls/pytango/-/merge_requests/673)
- [!681: Release GIL when adding/removing attributes, and add async methods](https://gitlab.com/tango-controls/pytango/-/merge_requests/681)
- [!693: Improve exception info when command execution failed](https://gitlab.com/tango-controls/pytango/-/merge_requests/693)
- [!702: Revert !664 (use attribute lock ATTR_BY_KERNEL for push_event)](https://gitlab.com/tango-controls/pytango/-/merge_requests/702)
- [!725: Remove notifd2db function](https://gitlab.com/tango-controls/pytango/-/merge_requests/725)
- [!735: Remove quality event](https://gitlab.com/tango-controls/pytango/-/merge_requests/735)
- [!737: Fix Database.get_device_attribute_property to mutate input dict](https://gitlab.com/tango-controls/pytango/-/merge_requests/737)

#### Added

- [!645: Extend pydevd debugging and coverage to dynamic attributes and commands](https://gitlab.com/tango-controls/pytango/-/merge_requests/645)
- [!652: Include stub file in wheels for better autocompletion (e.g., in VSCode)](https://gitlab.com/tango-controls/pytango/-/merge_requests/652)
- [!660: Add alarm event support](https://gitlab.com/tango-controls/pytango/-/merge_requests/660)
- [!667: Enable push events with python exceptions](https://gitlab.com/tango-controls/pytango/-/merge_requests/667)
- [!680: Update NumPy C API usage for 1.x and 2.0 compatibility](https://gitlab.com/tango-controls/pytango/-/merge_requests/680)
- [!686: Add option to set device description, status, state at device init](https://gitlab.com/tango-controls/pytango/-/merge_requests/686)
- [!691: Add "warning" method to tango logger](https://gitlab.com/tango-controls/pytango/-/merge_requests/691)
- [!698: Added DevInfo implementation (IDL 6)](https://gitlab.com/tango-controls/pytango/-/merge_requests/698)
- [!701: Export DeviceImpl.set_attribute_config, add tests for get/set_attribute_config at server side](https://gitlab.com/tango-controls/pytango/-/merge_requests/701)
- [!707: Warn if IDL struct interfaces like AttributeConfig are modified](https://gitlab.com/tango-controls/pytango/-/merge_requests/707)
- [!708: Add OpenTelemetry support for distributed tracing](https://gitlab.com/tango-controls/pytango/-/merge_requests/708)
- [!746: Add support for telemetry exporter 'none'](https://gitlab.com/tango-controls/pytango/-/merge_requests/746)

#### Fixed

- [!633: Fix DeviceProxy asynch attribute access with green modes, fix write_attribute(s)\_reply push model](https://gitlab.com/tango-controls/pytango/-/merge_requests/633)
- [!649: Fix high-level attribute read for asyncio DeviceProxies](https://gitlab.com/tango-controls/pytango/-/merge_requests/644)
- [!662: Fix Segfault in push_archive_event(self, attr_name) with attr_name != state or status](https://gitlab.com/tango-controls/pytango/-/merge_requests/662)
- [!669: Fix \*\_asynch methods on DeviceProxy](https://gitlab.com/tango-controls/pytango/-/merge_requests/669)
- [!672: Fix db.delete_device_attribute_property() if was called with several attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/672)
- [!674: Fix typing compatibility with numpy 1.20](https://gitlab.com/tango-controls/pytango/-/merge_requests/674)
- [!677: Fix class_property inheritance in high-level Device](https://gitlab.com/tango-controls/pytango/-/merge_requests/677)
- [!679: Resolve "Tango-Server crashes on Restart Command"](https://gitlab.com/tango-controls/pytango/-/merge_requests/679)
- [!699: Add typing support of DevVarLongStringArray, DevVarDoubleStringArray](https://gitlab.com/tango-controls/pytango/-/merge_requests/699)
- [!714: Fix memory leak in write str attribute](https://gitlab.com/tango-controls/pytango/-/merge_requests/714)
- [!715: Fix psutil DeprecationWarning](https://gitlab.com/tango-controls/pytango/-/merge_requests/715)
- [!727: Fix from_str_to_char leak from attribute name when pushing events](https://gitlab.com/tango-controls/pytango/-/merge_requests/727)
- [!730: Fix DbPutProperty in DataBaseDS (Python implementation)](https://gitlab.com/tango-controls/pytango/-/merge_requests/730)
- [!740: Fix some issues in DatabaseDS (Python implementation)](https://gitlab.com/tango-controls/pytango/-/merge_requests/740)
- [!745: Resolve "\_get_listening_tcp_ports in test_context can return wrong port"](https://gitlab.com/tango-controls/pytango/-/merge_requests/745)
- [!752: Fix some more issues in DatabaseDS (Python implementation)](https://gitlab.com/tango-controls/pytango/-/merge_requests/752)
- [!757: Fix even more issues in DatabaseDS (Python implementation)](https://gitlab.com/tango-controls/pytango/-/merge_requests/757)

#### Documentation

- [!642: Docs: Add note about push_event limitation with async green modes](https://gitlab.com/tango-controls/pytango/-/merge_requests/642)
- [!643: Docs: Fix CSS theme (bullet points, spacing, fonts)](https://gitlab.com/tango-controls/pytango/-/merge_requests/643)
- [!670: Re-organize docs](https://gitlab.com/tango-controls/pytango/-/merge_requests/670)
- [!683: BUILD.md: Add forgotten recurse-submodules for clone](https://gitlab.com/tango-controls/pytango/-/merge_requests/683)
- [!687: Docs: fix typos in client and server tutorials](https://gitlab.com/tango-controls/pytango/-/merge_requests/687)
- [!697: Docs: Rename Advanced to How-to guides](https://gitlab.com/tango-controls/pytango/-/merge_requests/697)
- [!716: Docs: Prepare for 10.0.0rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/716)
- [!734: Docs: update for 10.0.0rc2 release](https://gitlab.com/tango-controls/pytango/-/merge_requests/734)
- [!747: Docs: update for 10.0.0rc3 release](https://gitlab.com/tango-controls/pytango/-/merge_requests/747)
- [!754: Docs: update for 10.0.0rc4 release](https://gitlab.com/tango-controls/pytango/-/merge_requests/754)
- [!756: Docs: add information about DatabaseDS (Python implementation)](https://gitlab.com/tango-controls/pytango/-/merge_requests/756)
- [!758: Docs: update for 10.0.0 release, remove .devcontainer](https://gitlab.com/tango-controls/pytango/-/merge_requests/758)

#### DevOps and code maintenance changes

- [!640: CI: show Python and C++ stack trace if test segfaulted](https://gitlab.com/tango-controls/pytango/-/merge_requests/640)
- [!641: TangoCMakeModules: Add it to standardise build process](https://gitlab.com/tango-controls/pytango/-/merge_requests/641)
- [!644: ext/precompiled_header.hpp: Remove warnings about deprecated boost headers](https://gitlab.com/tango-controls/pytango/-/merge_requests/649)
- [!650: set target_compile_features to c++17](https://gitlab.com/tango-controls/pytango/-/merge_requests/650)
- [!651: Fix client default info repr test with cppTango v10](https://gitlab.com/tango-controls/pytango/-/merge_requests/651)
- [!653: Add simple tests for SQLite3 database (including some fixes)](https://gitlab.com/tango-controls/pytango/-/merge_requests/653)
- [!655: Fix pipeline for test sqlite database](https://gitlab.com/tango-controls/pytango/-/merge_requests/655)
- [!658: Bump cpptango ver to 10.0.0dev0 in CI](https://gitlab.com/tango-controls/pytango/-/merge_requests/658)
- [!665: Simplify Device_XImpl C++ code](https://gitlab.com/tango-controls/pytango/-/merge_requests/665)
- [!666: Fix PyTango tests fail after TangoTest!52](https://gitlab.com/tango-controls/pytango/-/merge_requests/666)
- [!668: Update Linux wheel Docker image to 1.6.0.dev1 for TangoTest echo_mode](https://gitlab.com/tango-controls/pytango/-/merge_requests/668)
- [!678: Skip test_async_exception_in_callback](https://gitlab.com/tango-controls/pytango/-/merge_requests/678)
- [!682: Add more pre-commit hooks and fix various found issues](https://gitlab.com/tango-controls/pytango/-/merge_requests/682)
- [!685: Rename TestDevice to DeviceToTest in test_database.py](https://gitlab.com/tango-controls/pytango/-/merge_requests/685)
- [!695: Updated TangoCMakeModules to the current head](https://gitlab.com/tango-controls/pytango/-/merge_requests/695)
- [!696: Add a manual task to run tests against specific branches](https://gitlab.com/tango-controls/pytango/-/merge_requests/696)
- [!700: CI: Cache pre-commit installation](https://gitlab.com/tango-controls/pytango/-/merge_requests/700)
- [!703: CI: Update Linux wheel Docker image to 1.6.0.dev2, and TangoTest](https://gitlab.com/tango-controls/pytango/-/merge_requests/703)
- [!704: Fix test_device_set_attr_config](https://gitlab.com/tango-controls/pytango/-/merge_requests/704)
- [!705: Fix gitlab-triage job](https://gitlab.com/tango-controls/pytango/-/merge_requests/705)
- [!707: CI update](https://gitlab.com/tango-controls/pytango/-/merge_requests/707)
- [!709: Add pixi as alternative to develop locally](https://gitlab.com/tango-controls/pytango/-/merge_requests/709)
- [!710: CI update](https://gitlab.com/tango-controls/pytango/-/merge_requests/710)
- [!711: Update pixi.lock (cpptango 10.0.0rc1, OpenTelemetry)](https://gitlab.com/tango-controls/pytango/-/merge_requests/711)
- [!712: CI: update to cpptango 10.0.0rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/712)
- [!713: Build against NumPy 2.0 by default](https://gitlab.com/tango-controls/pytango/-/merge_requests/713)
- [!717: Include extension debug info, and release additional debug Linux wheels](https://gitlab.com/tango-controls/pytango/-/merge_requests/717)
- [!718: Update Windows and Linux wheels to cppTango 10.0.0rc2](https://gitlab.com/tango-controls/pytango/-/merge_requests/718)
- [!719: CI: Use cppTango 10.0.0rc2 for sdist tests](https://gitlab.com/tango-controls/pytango/-/merge_requests/719)
- [!720: Bump version to v10.0.0rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/720)
- [!721: CI: Make separate Windows and macOS tests manual only](https://gitlab.com/tango-controls/pytango/-/merge_requests/721)
- [!722: Bump 10.0.0dev version](https://gitlab.com/tango-controls/pytango/-/merge_requests/722)
- [!723: CI: Refactor Gitlab CI into multiple files and fix release to pypi job](https://gitlab.com/tango-controls/pytango/-/merge_requests/723)
- [!724: Update pixi.lock for cpptango 10.0.0rc2](https://gitlab.com/tango-controls/pytango/-/merge_requests/724)
- [!726: Improve cpp code style](https://gitlab.com/tango-controls/pytango/-/merge_requests/726)
- [!728: CI: Fix problems with typing stub generation](https://gitlab.com/tango-controls/pytango/-/merge_requests/728)
- [!731: Add precompiled_header.hpp to target_precompile_headers](https://gitlab.com/tango-controls/pytango/-/merge_requests/731)
- [!732: CI: update to cppTango 10.0.0-rc3](https://gitlab.com/tango-controls/pytango/-/merge_requests/732)
- [!733: CI: update pixi to cppTango 10.0.0-rc3](https://gitlab.com/tango-controls/pytango/-/merge_requests/733)
- [!736: CI: update to cppTango 10.0.0-rc4](https://gitlab.com/tango-controls/pytango/-/merge_requests/736)
- [!739: CI: Add basic valgrind test](https://gitlab.com/tango-controls/pytango/-/merge_requests/739)
- [!741: CI: Add sphinx-lint to pre-commit](https://gitlab.com/tango-controls/pytango/-/merge_requests/741)
- [!743: CI: Replace "build" conda package with "python-build"](https://gitlab.com/tango-controls/pytango/-/merge_requests/743)
- [!744: CI: update to cppTango 10.0.0-rc5](https://gitlab.com/tango-controls/pytango/-/merge_requests/744)
- [!746: CI: fixed mixed server (TangoTest.so) test and minor CI issues](https://gitlab.com/tango-controls/pytango/-/merge_requests/746)
- [!748: CI: Use PyPI Trusted Publisher](https://gitlab.com/tango-controls/pytango/-/merge_requests/748)
- [!751: CI: update to cppTango 10.0.0-rc6](https://gitlab.com/tango-controls/pytango/-/merge_requests/751)
- [!753: CI: add one retry of failed tests](https://gitlab.com/tango-controls/pytango/-/merge_requests/753)
- [!755: CI: update to cpptango 10.0.0 and TangoTest 3.10](https://gitlab.com/tango-controls/pytango/-/merge_requests/755)

More details in the [full changelog 9.5.1...10.0.0](https://gitlab.com/tango-controls/pytango/-/compare/v9.5.1...v10.0.0)

______________________________________________________________________

### 9.5.1

#### Backports

- [!688: 9.5.x maintenance: Backport MRs 631, 644, 645, 664, 673, 674, 677](https://gitlab.com/tango-controls/pytango/-/merge_requests/688)

#### Changed

- [!684: 9.5.x maintenance: restrict NumPy to 1.x for 9.5.1 release](https://gitlab.com/tango-controls/pytango/-/merge_requests/684)
- Backport [!631: Improve error message if cannot convert value to DevBoolean type](https://gitlab.com/tango-controls/pytango/-/merge_requests/631)
- Backport [!645: Extend pydevd debugging and coverage to dynamic attributes and commands](https://gitlab.com/tango-controls/pytango/-/merge_requests/645)
- Backport [!673: Improve error message if python float value if written to int attribute for python >= 3.10](https://gitlab.com/tango-controls/pytango/-/merge_requests/673)

#### Fixed

- Backport [!644: Fix high-level attribute read for asyncio DeviceProxies](https://gitlab.com/tango-controls/pytango/-/merge_requests/644)
- Backport [!664: Use attribute lock ATTR_BY_KERNEL for push_event (fix a crash with asyncio green modes)](https://gitlab.com/tango-controls/pytango/-/merge_requests/664)
- Backport [!674: Fix typing compatibility with numpy 1.20](https://gitlab.com/tango-controls/pytango/-/merge_requests/674)
- Backport [!677: Fix class_property inheritance in high-level Device](https://gitlab.com/tango-controls/pytango/-/merge_requests/677)

#### Documentation

- [!689: 9.5.x maintenance: update docs and migration guide for 9.5.1-rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/689)
- [!690: 9.5.x maintenance: update docs bump for 9.5.1](https://gitlab.com/tango-controls/pytango/-/merge_requests/690)

More details in the [full changelog 9.5.0...9.5.1](https://gitlab.com/tango-controls/pytango/-/compare/v9.5.0...v9.5.1)

______________________________________________________________________

### 9.5.0

#### Changed

- [!558: Check if user's class methods are coroutines in Async mode](https://gitlab.com/tango-controls/pytango/-/merge_requests/558)
- [!614: Require cppTango 9.5.0, bump to 9.5.0.dev0, doc fixes](https://gitlab.com/tango-controls/pytango/-/merge_requests/614)
- [!617: Use 127.0.0.1 as default TestContext host instead of external IP](https://gitlab.com/tango-controls/pytango/-/merge_requests/617)

#### Added

- [!388: Enable short-name access to TestContext devices](https://gitlab.com/tango-controls/pytango/-/merge_requests/388)
- [!568: Declaration of properties, attributes and command type with typing hints](https://gitlab.com/tango-controls/pytango/-/merge_requests/568)
- [!580: IMAGEs support added to set_write_value](https://gitlab.com/tango-controls/pytango/-/merge_requests/580)
- [!581: Support forwarded attributes in TestContext](https://gitlab.com/tango-controls/pytango/-/merge_requests/581)
- [!582: Add support for EncodedAttribute in high-level API device](https://gitlab.com/tango-controls/pytango/-/merge_requests/582)
- [!592: Expose complete API of DeviceImpl.remove_attribute()](https://gitlab.com/tango-controls/pytango/-/merge_requests/592)
- [!616: Support server debugging with PyCharm and VS Code (pydevd)](https://gitlab.com/tango-controls/pytango/-/merge_requests/616)
- [!618: Resolve "Python 3.12 support"](https://gitlab.com/tango-controls/pytango/-/merge_requests/618)

#### Fixed

- [!591: Handle spaces in Python path in winsetup (Windows only)](https://gitlab.com/tango-controls/pytango/-/merge_requests/591)
- [!600: Fix green_mode bug in TestContext](https://gitlab.com/tango-controls/pytango/-/merge_requests/600)
- [!612: Close socket in get_host_ip() (as used by DeviceTestContext)](https://gitlab.com/tango-controls/pytango/-/merge_requests/612)
- [!625: Ignore gevent when using TestContext if not installed](https://gitlab.com/tango-controls/pytango/-/merge_requests/625)
- [!627: Fix problem if self has a type hint](https://gitlab.com/tango-controls/pytango/-/merge_requests/627)
- [!634: Fix SegFault if set_value was called with None](https://gitlab.com/tango-controls/pytango/-/merge_requests/634)

#### Removed

- [!602: Remove CmdArgType.DevInt (cppTango DEV_INT)](https://gitlab.com/tango-controls/pytango/-/merge_requests/602)
- [!604: Deprecated signature for WAttribute.get_write_value() removed](https://gitlab.com/tango-controls/pytango/-/merge_requests/604)

#### Documentation

- [!615: Update docs and migration guide for 9.5.0-rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/615)
- [!628: Update docs and bump for 9.5.0rc2](https://gitlab.com/tango-controls/pytango/-/merge_requests/628)
- [!636: Update docs and bump for 9.5.0rc3](https://gitlab.com/tango-controls/pytango/-/merge_requests/636)
- [!638: Update docs and bump for 9.5.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/638)

#### DevOps and code maintenance changes

- [!569: New build system using cmake](https://gitlab.com/tango-controls/pytango/-/merge_requests/569)
- [!590: Test all wheels as part of default branch CI](https://gitlab.com/tango-controls/pytango/-/merge_requests/590)
- [!596: Bump 9.4.3 dev version](https://gitlab.com/tango-controls/pytango/-/merge_requests/596)
- [!598: Compile TangoTest so test-main-cpptango uses latest cpptango](https://gitlab.com/tango-controls/pytango/-/merge_requests/598)
- [!599: Fix micromamba installation](https://gitlab.com/tango-controls/pytango/-/merge_requests/599)
- [!601: Update triage message](https://gitlab.com/tango-controls/pytango/-/merge_requests/601)
- [!603: Test against cpptango 9.5.0rc2](https://gitlab.com/tango-controls/pytango/-/merge_requests/603)
- [!605: Solve init hook test error on windows](https://gitlab.com/tango-controls/pytango/-/merge_requests/605)
- [!606: Test against cpptango 9.5.0rc3](https://gitlab.com/tango-controls/pytango/-/merge_requests/606)
- [!607: CI: Allow numpy without blas when testing Linux i686 wheels](https://gitlab.com/tango-controls/pytango/-/merge_requests/607)
- [!608: Cleanup outdated code](https://gitlab.com/tango-controls/pytango/-/merge_requests/608)
- [!609: Skip more tests on AppVeyor](https://gitlab.com/tango-controls/pytango/-/merge_requests/609)
- [!610: Update cpptango to 9.5.0rc4](https://gitlab.com/tango-controls/pytango/-/merge_requests/610)
- [!611: Mark test_server_init_hook_subscribe_event_multiple_devices xfail](https://gitlab.com/tango-controls/pytango/-/merge_requests/611)
- [!613: Build against cpptango 9.5.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/613)
- [!619: Bump pytango-builder image (libzmq-4.3.5 for Linux wheels)](https://gitlab.com/tango-controls/pytango/-/merge_requests/619)
- [!620: Reduce compiler warnings and pin scikit-build-core](https://gitlab.com/tango-controls/pytango/-/merge_requests/620)
- [!621: Change relative imports to absolute and unpin scikit-build-core](https://gitlab.com/tango-controls/pytango/-/merge_requests/621)
- [!622: Update Boost to 1.83.0 for Windows builds](https://gitlab.com/tango-controls/pytango/-/merge_requests/622)
- [!623: CI: Allow numpy without blas when building Linux i686 Py 3.12 wheels](https://gitlab.com/tango-controls/pytango/-/merge_requests/623)
- [!624: Force C++ standard to 14](https://gitlab.com/tango-controls/pytango/-/merge_requests/624)
- [!629: CI: Add support for zmq 4.3.5 in Windows builds](https://gitlab.com/tango-controls/pytango/-/merge_requests/629)
- [!630: CI: Enable DESY Windows Runner](https://gitlab.com/tango-controls/pytango/-/merge_requests/630)
- [!632: CI: Use latest TangoTest for linux:test-main-cpptango job](https://gitlab.com/tango-controls/pytango/-/merge_requests/632)
- [!635: CI: Update macOS image used](https://gitlab.com/tango-controls/pytango/-/merge_requests/635)

More details in the [full changelog 9.4.2...9.5.0](https://gitlab.com/tango-controls/pytango/-/compare/v9.4.2...v9.5.0)

______________________________________________________________________

### 9.4.2

#### Features

- [!578: Add server init hook to high-level and low-level devices](https://gitlab.com/tango-controls/pytango/-/merge_requests/578)
- [!562: Check code coverage](https://gitlab.com/tango-controls/pytango/-/merge_requests/562)
- [!577: Implement new python and NumPy version policy](https://gitlab.com/tango-controls/pytango/-/merge_requests/577)

#### Bug fixes and changes

- [!551: Handle unsupported DeviceTestContext properties gracefully](https://gitlab.com/tango-controls/pytango/-/merge_requests/551)
- [!556: Fix source location recorded by logging decorators](https://gitlab.com/tango-controls/pytango/-/merge_requests/556)
- [!564: Asyncio server doesn't change state to ALARM with AttrQuality](https://gitlab.com/tango-controls/pytango/-/merge_requests/564)
- [!557: Fix DevEncoded attributes and commands](https://gitlab.com/tango-controls/pytango/-/merge_requests/557)
- [!565: Raise UnicodeError instead of segfaulting when Latin-1 encoding fails](https://gitlab.com/tango-controls/pytango/-/merge_requests/565)
- [!570: Fix linter problem in winsetup.py](https://gitlab.com/tango-controls/pytango/-/merge_requests/570)
- [!579: Extend "empty string workaround" to sequences for DeviceTestContext properties](https://gitlab.com/tango-controls/pytango/-/merge_requests/579)

#### Doc fixes

- [!571: Update new build system doc](https://gitlab.com/tango-controls/pytango/-/merge_requests/571)
- [!572: Improve docs for push_data_ready_event and EnsureOmniThread](https://gitlab.com/tango-controls/pytango/-/merge_requests/572)
- [!587: Update docs and bump version for 9.4.2rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/587)
- [!595: Fixed history of changes](https://gitlab.com/tango-controls/pytango/-/merge_requests/595)

DevOps changes:
\- [!563: Skip log location tests in AppVeyor CI](https://gitlab.com/tango-controls/pytango/-/merge_requests/563)
\- [!566: Add AppVeyor Windows builds for Python 3.9 to 3.11, Boost 1.82.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/566)
\- [!575: Add job to test main cpptango branch](https://gitlab.com/tango-controls/pytango/-/merge_requests/575)
\- [!574: Added test for checking default and non-default units](https://gitlab.com/tango-controls/pytango/-/merge_requests/574)
\- [!576: Add macOS wheels + gitlab-ci cleaning](https://gitlab.com/tango-controls/pytango/-/merge_requests/576)
\- [!585: Move to cppTango 9.4.2, drop Python\<3.9 on Win, update wheel deps](https://gitlab.com/tango-controls/pytango/-/merge_requests/585)
\- [!588: Skip failing test in Winodws](https://gitlab.com/tango-controls/pytango/-/merge_requests/588)
\- [!593: Test_server_init_hook_subscribe_event_multiple_devices skipped](https://gitlab.com/tango-controls/pytango/-/merge_requests/593)

More details in the [full changelog 9.4.1...9.4.2](https://gitlab.com/tango-controls/pytango/-/compare/v9.4.1...v9.4.2)

______________________________________________________________________

### 9.4.1

#### Bug fixes and changes

- [!547: Fix attributes with device inheritance and repeated method wrapping regression in 9.4.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/547)
- [!548: Fix decorated attribute methods regression in 9.4.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/548)

#### Doc fixes

- [!546: Add note about pip version for binary packages](https://gitlab.com/tango-controls/pytango/-/merge_requests/546)
- [!544: Bump version to 9.4.1dev0](https://gitlab.com/tango-controls/pytango/-/merge_requests/544)
- [!555: Update docs and bump version for 9.4.0rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/555)
- [!559: Groom docstrings](https://gitlab.com/tango-controls/pytango/-/merge_requests/559)
- [!560: Bump for 9.4.1](https://gitlab.com/tango-controls/pytango/-/merge_requests/560)

Deprecation fixes:
\- [!553: Remove compiler version check from setup.py](https://gitlab.com/tango-controls/pytango/-/merge_requests/553)

DevOps changes:
\- [!545: Run black on repo and add to pre-commit-config](https://gitlab.com/tango-controls/pytango/-/merge_requests/545)
\- [!554: Update to omniorb 4.2.5 for Linux wheels](https://gitlab.com/tango-controls/pytango/-/merge_requests/554)
\- [!549: Use new tango-controls group runners](https://gitlab.com/tango-controls/pytango/-/merge_requests/549)
\- [!550: Update mambaforge image and use conda instead of apt packages in CI](https://gitlab.com/tango-controls/pytango/-/merge_requests/550)
\- [!552: Run gitlab-triage to update old issues/MRs](https://gitlab.com/tango-controls/pytango/-/merge_requests/552)

More details in the [full changelog 9.4.0...9.4.1](https://gitlab.com/tango-controls/pytango/-/compare/v9.4.0...v9.4.1)

______________________________________________________________________

### 9.4.0

:::{warning}
not recommended due to significant regressions
:::

#### Features

- [!522: Support of non-bound methods for attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/522)
- [!535: Allow developer to optionally add attributes to a DeviceProxy instance](https://gitlab.com/tango-controls/pytango/-/merge_requests/535)
- [!515: DevEnum spectrum and image attributes support added](https://gitlab.com/tango-controls/pytango/-/merge_requests/515)
- [!502: Provide binary wheels on PyPI using pytango-builder images](https://gitlab.com/tango-controls/pytango/-/merge_requests/502)
- [!510: Added high level API for dynamic attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/510)
- [!511: Added fisallowed kwarg for static/dynamic commands and is_allowed method for dynamic commands](https://gitlab.com/tango-controls/pytango/-/merge_requests/511)
- [!528: Added getter, read and is_allowed attribute decorators](https://gitlab.com/tango-controls/pytango/-/merge_requests/528)
- [!542: Improve device types autocompletion in IDEs](https://gitlab.com/tango-controls/pytango/-/merge_requests/542)

#### Changes

- [!490: Drop Python 2.7 and 3.5 support](https://gitlab.com/tango-controls/pytango/-/merge_requests/490)
- [!486: Switch support from cppTango 9.3 to 9.4](https://gitlab.com/tango-controls/pytango/-/merge_requests/486)
- [!536: Require cppTango>=9.4.1 to import the library](https://gitlab.com/tango-controls/pytango/-/merge_requests/536)
- [!489: Make numpy a hard requirement](https://gitlab.com/tango-controls/pytango/-/merge_requests/489)
- [!493: Improve spectrum and image attribute behaviour with empty lists (breaking change to API!)](https://gitlab.com/tango-controls/pytango/-/merge_requests/493)
- [!492: Change DServer inheritance from Device_4Impl to Device_5Impl](https://gitlab.com/tango-controls/pytango/-/merge_requests/492)
- [!514: Remove Python 2 compatibility code](https://gitlab.com/tango-controls/pytango/-/merge_requests/514)
- [!539: Update CI to cppTango 9.4.1, change default ORBendpoint host to 0.0.0.0, fix tests](https://gitlab.com/tango-controls/pytango/-/merge_requests/539)
- [!541: Workaround cppTango#1055 for DatabaseDS startup](https://gitlab.com/tango-controls/pytango/-/merge_requests/541)

#### Bug fixes

- [!495: Fix log streams with % and no args](https://gitlab.com/tango-controls/pytango/-/merge_requests/495)
- [!516: Resolve "Crash when writing numpy.array to DeviceProxy string array attributes"](https://gitlab.com/tango-controls/pytango/-/merge_requests/516)
- [!533: Fix high-level enum read exception when quality is ATTR_INVALID](https://gitlab.com/tango-controls/pytango/-/merge_requests/533)

#### Doc fixes

- [!505: Fix some docs related to Tango.Util](https://gitlab.com/tango-controls/pytango/-/merge_requests/505)
- [!523: Document set_write_value WAttribute method](https://gitlab.com/tango-controls/pytango/-/merge_requests/523)
- [!524: Fixed documentation for DeviceProxy.get_attribute_config_ex](https://gitlab.com/tango-controls/pytango/-/merge_requests/524)
- [!526: Clarify gevent dependency](https://gitlab.com/tango-controls/pytango/-/merge_requests/526)
- [!487: Bump for 9.4.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/487)
- [!500: Update docs for 9.3 to 9.4 changes](https://gitlab.com/tango-controls/pytango/-/merge_requests/500)
- [!501: Update version for 9.4.0rc1](https://gitlab.com/tango-controls/pytango/-/merge_requests/501)
- [!507: Update version for 9.4.0rc2](https://gitlab.com/tango-controls/pytango/-/merge_requests/507)
- [!538: Update version for 9.4.0rc3](https://gitlab.com/tango-controls/pytango/-/merge_requests/538)
- [!512: Add some more training examples](https://gitlab.com/tango-controls/pytango/-/merge_requests/512)
- [!540: Update docs and bump version for 9.4.0 release](https://gitlab.com/tango-controls/pytango/-/merge_requests/540)

Deprecation fixes:
\- [!491: Remove unused method CppDeviceClassWrap::delete_class](https://gitlab.com/tango-controls/pytango/-/merge_requests/491)
\- [!494: Remove unnecessary constants TANGO_LONG32 and TANGO_LONG64](https://gitlab.com/tango-controls/pytango/-/merge_requests/494)
\- [!497: Replace pytest-xdist with pytest-forked for testing](https://gitlab.com/tango-controls/pytango/-/merge_requests/497)
\- [!498: Fix Python 3.11 compatibility](https://gitlab.com/tango-controls/pytango/-/merge_requests/498)
\- [!513: Replace deprecated distutils.version](https://gitlab.com/tango-controls/pytango/-/merge_requests/513)
\- [!534: Replace deprecated numpy.bool8 alias](https://gitlab.com/tango-controls/pytango/-/merge_requests/534)

DevOps changes:
\- [!531: Configure unit tests report in gitlab-ci](https://gitlab.com/tango-controls/pytango/-/merge_requests/531)
\- [!532: Run ruff via pre-commit](https://gitlab.com/tango-controls/pytango/-/merge_requests/532)
\- [!519: Testing: improve error message for event test failures](https://gitlab.com/tango-controls/pytango/-/merge_requests/519)
\- [!530: Unnecessary tests removed](https://gitlab.com/tango-controls/pytango/-/merge_requests/530)
\- [!496: Force numpy installation with help of pyproject.toml (PEP 518) before build](https://gitlab.com/tango-controls/pytango/-/merge_requests/496)
\- [!509: Prefer binary dependencies for test-wheel](https://gitlab.com/tango-controls/pytango/-/merge_requests/509)
\- [!508: Allow failure for aarch64 test](https://gitlab.com/tango-controls/pytango/-/merge_requests/508)
\- [!488: Add cpptango_rc to the Dockerfile](https://gitlab.com/tango-controls/pytango/-/merge_requests/488)
\- [!520: Devcontainer fix for Mac M1 host](https://gitlab.com/tango-controls/pytango/-/merge_requests/520)
\- [!525: Git ignore code-workspace and .DS_Store files](https://gitlab.com/tango-controls/pytango/-/merge_requests/525)
\- [!499: Disable AppVeyor but keep the config file](https://gitlab.com/tango-controls/pytango/-/merge_requests/499)
\- [!503: Disable AppVeyor builds temporarily](https://gitlab.com/tango-controls/pytango/-/merge_requests/503)
\- [!504: Update AppVeyor CI for cppTango 9.4.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/504)
\- [!506: Update AppVeyor cppTango 9.4.0.windows1 package](https://gitlab.com/tango-controls/pytango/-/merge_requests/506)
\- [!543: Fix readme syntax and add twine check](https://gitlab.com/tango-controls/pytango/-/merge_requests/543)

More details in the [full changelog 9.3.6...9.4.0](https://gitlab.com/tango-controls/pytango/-/compare/v9.3.6...v9.4.0)

______________________________________________________________________

### 9.3.6

#### Changes

- [Pull Request #482: Use cpptango 9.3.5 for Widows wheels (except Py27 x64)](https://gitlab.com/tango-controls/pytango/-/merge_requests/482)

#### Bug fixes

- [Pull Request #477: Resolve "Dynamic attribute in 9.3.5 fails"](https://gitlab.com/tango-controls/pytango/-/merge_requests/477)
- [Pull Request #479: Fix green mode usage from run method kwarg](https://gitlab.com/tango-controls/pytango/-/merge_requests/479)
- [Pull Request #480: Resolve "read-only dynamic attribute with dummy write function fails in 9.3.5"](https://gitlab.com/tango-controls/pytango/-/merge_requests/480)

______________________________________________________________________

### 9.3.5

#### Features

- [Pull Request #470: Add set_data_ready_event method to Device](https://gitlab.com/tango-controls/pytango/-/merge_requests/470)

#### Changes

- [Pull Request #471: Fail if mixed green modes used in device server](https://gitlab.com/tango-controls/pytango/-/merge_requests/471)

#### Bug fixes

- [Pull Request #461: Fix handling of -ORBEndPointX command line options](https://gitlab.com/tango-controls/pytango/-/merge_requests/461)
- [Pull Request #462: Ensure PYTANGO_NUMPY_VERSION is stringized to support newer C++ compilers](https://gitlab.com/tango-controls/pytango/-/merge_requests/462)
- [Pull Request #465: Restore dynamic attribute functionality with unbound methods](https://gitlab.com/tango-controls/pytango/-/merge_requests/465)
- [Pull Request #466: Explicit boost::python::optional template usage to fix compilation with gcc>10](https://gitlab.com/tango-controls/pytango/-/merge_requests/466)

#### Doc fixes

- [Pull Request #467: Better MultiDeviceTestContext workaround](https://gitlab.com/tango-controls/pytango/-/merge_requests/467)
- [Pull Request #474: Update documentation for tango.Database](https://gitlab.com/tango-controls/pytango/-/merge_requests/474)

DevOps features:
\- [Pull Request #473: Make universal dockerfile](https://gitlab.com/tango-controls/pytango/-/merge_requests/473)

______________________________________________________________________

### 9.3.4

#### Changes

- [Pull Request #430: Raise when setting non-existent DeviceProxy attr](https://gitlab.com/tango-controls/pytango/-/merge_requests/430)
- [Pull Request #444: Add "friendly" argparser for device server arguments (#132, #354)](https://gitlab.com/tango-controls/pytango/-/merge_requests/444)

#### Bug fixes

- [Pull Request #401: Fix read/write/is_allowed not called for dynamic attribute in async mode server (#173)](https://gitlab.com/tango-controls/pytango/-/merge_requests/401)
- [Pull Request #417: Fix DeviceProxy constructor reference cycle (#412)](https://gitlab.com/tango-controls/pytango/-/merge_requests/417)
- [Pull Request #418: Release GIL in DeviceProxy and AttributeProxy dtor](https://gitlab.com/tango-controls/pytango/-/merge_requests/418)
- [Pull Request #434: Fix Device green_mode usage in MultiDeviceTestContext](https://gitlab.com/tango-controls/pytango/-/merge_requests/434)
- [Pull Request #436: Fix MSVC 9 syntax issue with shared pointer deletion](https://gitlab.com/tango-controls/pytango/-/merge_requests/436)
- [Pull Request #438: Add unit tests for device server logging](https://gitlab.com/tango-controls/pytango/-/merge_requests/438)
- [Pull Request #446: Allow pipes to be inherited by Device subclasses (#439)](https://gitlab.com/tango-controls/pytango/-/merge_requests/446)

#### Deprecation fixes

- [Pull Request #414: Fix deprecated warning with numpy 1.20](https://gitlab.com/tango-controls/pytango/-/merge_requests/414)
- [Pull Request #424: tango/pytango_pprint.py: Use correct syntax for comparing object contents](https://gitlab.com/tango-controls/pytango/-/merge_requests/424)
- [Pull Request #425: Fix some and silence some C++ compiler warnings](https://gitlab.com/tango-controls/pytango/-/merge_requests/425)
- [Pull Request #439: Fix asyncio Python 3.10 compatibility (#429)](https://gitlab.com/tango-controls/pytango/-/merge_requests/439)
- [Pull Request #449: Use Py_ssize_t for all CPython indexing](https://gitlab.com/tango-controls/pytango/-/merge_requests/449)

#### Doc fixes

- [Pull Request #404: Typo on Sphinx documentation (#173)](https://gitlab.com/tango-controls/pytango/-/merge_requests/404)
- [Pull Request #406: Fix docs - missing DbDevExportInfos and DbDevImportInfos](https://gitlab.com/tango-controls/pytango/-/merge_requests/406)
- [Pull Request #420: Fix broken link: no s in gevent](https://gitlab.com/tango-controls/pytango/-/merge_requests/420)
- [Pull Request #422: Uncomment docs of tango.Util.instance() and build docs for other static methods](https://gitlab.com/tango-controls/pytango/-/merge_requests/422)
- [Pull Request #426: [docs] Fixed arguments name when calling command decorator](https://gitlab.com/tango-controls/pytango/-/merge_requests/426)
- [Pull Request #427: [docs] Fixed variables name in a tango.Database.add_server method example](https://gitlab.com/tango-controls/pytango/-/merge_requests/427)
- [Pull Request #429: Add training material examples](https://gitlab.com/tango-controls/pytango/-/merge_requests/429)
- [Pull Request #433: Fix server method in DevEnum example in doc/data_types.rst](https://gitlab.com/tango-controls/pytango/-/merge_requests/433)
- [Pull Request #440: Resolve "Missing methods in Documentation" (#217)](https://gitlab.com/tango-controls/pytango/-/merge_requests/440)
- [Pull Request #442: Invalid escape fix](https://gitlab.com/tango-controls/pytango/-/merge_requests/442)
- [Pull Request #453: Remove docs generation from build](https://gitlab.com/tango-controls/pytango/-/merge_requests/453)
- [Pull Request #454: Debian/Ubuntu installation docs updated](https://gitlab.com/tango-controls/pytango/-/merge_requests/454)
- [Pull Request #455: Update contribution guidelines, drop stable branch](https://gitlab.com/tango-controls/pytango/-/merge_requests/455)

#### DevOps fixes

- [Pull Request #409: Enable CI/CD in Gitlab (#399)](https://gitlab.com/tango-controls/pytango/-/merge_requests/409)
- [Pull Request #410: Replace github links](https://gitlab.com/tango-controls/pytango/-/merge_requests/410)
- [Pull Request #411: Build and upload source distribution to pypi](https://gitlab.com/tango-controls/pytango/-/merge_requests/411)
- [Pull Request #423: Use numpy parallel compilation if available (#416)](https://gitlab.com/tango-controls/pytango/-/merge_requests/423)
- [Pull Request #428: Gitlab CI image build + push](https://gitlab.com/tango-controls/pytango/-/merge_requests/428)
- [Pull Request #445: Split Gitlab CI caches per job](https://gitlab.com/tango-controls/pytango/-/merge_requests/445)
- [Pull Request #448: Add missing cmake files to sdist](https://gitlab.com/tango-controls/pytango/-/merge_requests/448)

______________________________________________________________________

### 9.3.3

#### Features

- [Pull Request #378: Add string support for MultiDeviceTestContext devices_info class field](https://gitlab.com/tango-controls/pytango/-/merge_requests/378)
- [Pull Request #384: Add test context support for memorized attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/384)
- [Pull Request #395: Fix Windows build and add CI test suite (#355, #368, #369)](https://gitlab.com/tango-controls/pytango/-/merge_requests/395)

#### Changes

- [Pull Request #365: Preserve cause of exception when getting/setting attribute in DeviceProxy (#364)](https://gitlab.com/tango-controls/pytango/-/merge_requests/365)
- [Pull Request #385: Improve mandatory + default device property error message (#380)](https://gitlab.com/tango-controls/pytango/-/merge_requests/385)
- [Pull Request #397: Add std namespace prefix in C++ code](https://gitlab.com/tango-controls/pytango/-/merge_requests/397)

#### Bug/doc fixes

- [Pull Request #360: Fix convert2array for Unicode to DevVarStringArray (Py3) (#361)](https://gitlab.com/tango-controls/pytango/-/merge_requests/360)
- [Pull Request #386: Fix DeviceProxy repr/str memory leak (#298)](https://gitlab.com/tango-controls/pytango/-/merge_requests/386)
- [Pull Request #352: Fix sphinx v3 warning](https://gitlab.com/tango-controls/pytango/-/merge_requests/352)
- [Pull Request #359: MultiDeviceTestContext example](https://gitlab.com/tango-controls/pytango/-/merge_requests/359)
- [Pull Request #363: Change old doc links from ESRF to RTD](https://gitlab.com/tango-controls/pytango/-/merge_requests/363)
- [Pull Request #370: Update CI to use cppTango 9.3.4rc6](https://gitlab.com/tango-controls/pytango/-/merge_requests/370)
- [Pull Request #389: Update CI and dev Docker to cpptango 9.3.4](https://gitlab.com/tango-controls/pytango/-/merge_requests/389)
- [Pull Request #376: Update Windows CI and dev containers to boost 1.73.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/376)
- [Pull Request #377: VScode remote development container support](https://gitlab.com/tango-controls/pytango/-/merge_requests/377)
- [Pull Request #391: Add documentation about testing](https://gitlab.com/tango-controls/pytango/-/merge_requests/391)
- [Pull Request #393: Fix a typo in get_server_info documentation (#392)](https://gitlab.com/tango-controls/pytango/-/merge_requests/393)

______________________________________________________________________

### 9.3.2

#### Features

- [Pull Request #314: Add MultiDeviceTestContext for testing more than one Device](https://gitlab.com/tango-controls/pytango/-/merge_requests/314)
- [Pull Request #317: Add get_device_attribute_list and missing pipe methods to Database interface (#313)](https://gitlab.com/tango-controls/pytango/-/merge_requests/317)
- [Pull Request #327: Add EnsureOmniThread and is_omni_thread (#307, #292)](https://gitlab.com/tango-controls/pytango/-/merge_requests/327)

#### Changes

- [Pull Request #316: Reduce six requirement from 1.12 to 1.10 (#296)](https://gitlab.com/tango-controls/pytango/-/merge_requests/316)
- [Pull Request #326: Add Docker development container](https://gitlab.com/tango-controls/pytango/-/merge_requests/326)
- [Pull Request #330: Add enum34 to Python 2.7 docker images](https://gitlab.com/tango-controls/pytango/-/merge_requests/330)
- [Pull Request #329: Add test to verify get_device_properties called on init](https://gitlab.com/tango-controls/pytango/-/merge_requests/329)
- [Pull Request #341: Build DevFailed origin from format_exception (#340)](https://gitlab.com/tango-controls/pytango/-/merge_requests/341)

#### Bug/doc fixes

- [Pull Request #301: Fix documentation error](https://gitlab.com/tango-controls/pytango/-/merge_requests/301)
- [Pull Request #334: Update green mode docs and asyncio example (#333)](https://gitlab.com/tango-controls/pytango/-/merge_requests/334)
- [Pull Request #335: Generalise search for libboost_python on POSIX (#300, #310)](https://gitlab.com/tango-controls/pytango/-/merge_requests/335)
- [Pull Request #343: Extend the info on dependencies in README](https://gitlab.com/tango-controls/pytango/-/merge_requests/343)
- [Pull Request #345: Fix power_supply client example PowerOn -> TurnOn](https://gitlab.com/tango-controls/pytango/-/merge_requests/345)
- [Pull Request #347: Fix memory leak for DevEncoded attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/347)
- [Pull Request #348: Fix dynamic enum attributes created without labels (#56)](https://gitlab.com/tango-controls/pytango/-/merge_requests/348)

______________________________________________________________________

### 9.3.1

#### Changes

- [Pull Request #277: Windows builds using AppVeyor (#176)](https://gitlab.com/tango-controls/pytango/-/merge_requests/277)
- [Pull Request #290: Update docs: int types maps to DevLong64 (#282)](https://gitlab.com/tango-controls/pytango/-/merge_requests/290)
- [Pull Request #293: Update exception types in proxy docstrings](https://gitlab.com/tango-controls/pytango/-/merge_requests/293)

#### Bug fixes

- [Pull Request #270: Add six >= 1.12 requirement (#269)](https://gitlab.com/tango-controls/pytango/-/merge_requests/270)
- [Pull Request #273: DeviceAttribute.is_empty not working correctly with latest cpp tango version (#271)](https://gitlab.com/tango-controls/pytango/-/merge_requests/273)
- [Pull Request #274: Add unit tests for spectrum attributes, including empty (#271)](https://gitlab.com/tango-controls/pytango/-/merge_requests/274)
- [Pull Request #281: Fix DevEncoded commands on Python 3 (#280)](https://gitlab.com/tango-controls/pytango/-/merge_requests/281)
- [Pull Request #288: Make sure we only convert to string python unicode/str/bytes objects (#285)](https://gitlab.com/tango-controls/pytango/-/merge_requests/288)
- [Pull Request #289: Fix compilation warnings and conda build (#286)](https://gitlab.com/tango-controls/pytango/-/merge_requests/289)

______________________________________________________________________

### 9.3.0

#### Changes

- [Pull Request #242: Improve Python version check for enum34 install](https://gitlab.com/tango-controls/pytango/-/merge_requests/242)
- [Pull Request #250: Develop 9.3.0](https://gitlab.com/tango-controls/pytango/-/merge_requests/250)
- [Pull Request #258: Change Travis CI builds to xenial](https://gitlab.com/tango-controls/pytango/-/merge_requests/258)

#### Bug fixes

- [Pull Request #245: Change for collections abstract base class](https://gitlab.com/tango-controls/pytango/-/merge_requests/245)
- [Pull Request #247: Use IP address instead of hostname (fix #246)](https://gitlab.com/tango-controls/pytango/-/merge_requests/247)
- [Pull Request #252: Fix wrong link to tango dependency (#235)](https://gitlab.com/tango-controls/pytango/-/merge_requests/252)
- [Pull Request #254: Fix mapping of AttrWriteType WT_UNKNOWN](https://gitlab.com/tango-controls/pytango/-/merge_requests/254)
- [Pull Request #257: Fix some docs and docstrings](https://gitlab.com/tango-controls/pytango/-/merge_requests/257)
- [Pull Request #260: add ApiUtil.cleanup()](https://gitlab.com/tango-controls/pytango/-/merge_requests/260)
- [Pull Request #262: Fix compile error under Linux](https://gitlab.com/tango-controls/pytango/-/merge_requests/262)
- [Pull Request #263: Fix #251: Python 2 vs Python 3: DevString with bytes](https://gitlab.com/tango-controls/pytango/-/merge_requests/263)

______________________________________________________________________

### 9.2.5

#### Changes

- [Pull Request #212: Skip databaseds backends in PyTango compatibility module](https://gitlab.com/tango-controls/pytango/-/merge_requests/212)
- [Pull Request #221: DevEnum attributes can now be directly assigned labels](https://gitlab.com/tango-controls/pytango/-/merge_requests/221)
- [Pull Request #236: Cleanup db_access module](https://gitlab.com/tango-controls/pytango/-/merge_requests/236)
- [Pull Request #237: Add info about how to release a new version](https://gitlab.com/tango-controls/pytango/-/merge_requests/237)

#### Bug fixes

- [Pull Request #209 (issue #207): Fix documentation warnings](https://gitlab.com/tango-controls/pytango/-/merge_requests/209)
- [Pull Request #211: Yet another fix to the gevent threadpool error wrapping](https://gitlab.com/tango-controls/pytango/-/merge_requests/211)
- [Pull Request #214 (issue #213): DevEncoded attribute should produce a bytes object in python 3](https://gitlab.com/tango-controls/pytango/-/merge_requests/214)
- [Pull Request #219: Fixing icons in documentation](https://gitlab.com/tango-controls/pytango/-/merge_requests/219)
- [Pull Request #220: Fix 'DevFailed' object does not support indexing](https://gitlab.com/tango-controls/pytango/-/merge_requests/220)
- [Pull Request #225 (issue #215): Fix exception propagation in python 3](https://gitlab.com/tango-controls/pytango/-/merge_requests/225)
- [Pull Request #226 (issue #216): Add missing converter from python bytes to char\*](https://gitlab.com/tango-controls/pytango/-/merge_requests/226)
- [Pull Request #227: Gevent issue #1260 should be fixed by now](https://gitlab.com/tango-controls/pytango/-/merge_requests/227)
- [Pull Request #232: use special case-insensitive weak values dictionary for Tango nodes](https://gitlab.com/tango-controls/pytango/-/merge_requests/232)

______________________________________________________________________

### 9.2.4

#### Changes

- [Pull Request #194 (issue #188): Easier access to DevEnum attribute using python enum](https://gitlab.com/tango-controls/pytango/-/merge_requests/194)
- [Pull Request #199 (issue #195): Support python enum as dtype argument for attributes](https://gitlab.com/tango-controls/pytango/-/merge_requests/199)
- [Pull Request #205 (issue #202): Python 3.7 compatibility](https://gitlab.com/tango-controls/pytango/-/merge_requests/205)

#### Bug fixes

- [Pull Request #193 (issue #192): Fix a gevent green mode memory leak introduced in v9.2.3](https://gitlab.com/tango-controls/pytango/-/merge_requests/193)

______________________________________________________________________

### 9.2.3

#### Changes

- [Pull Request #169: Use tango-controls theme for the documentation](https://gitlab.com/tango-controls/pytango/-/merge_requests/169)
- [Pull Request #170 (issue #171): Use a private gevent ThreadPool](https://gitlab.com/tango-controls/pytango/-/merge_requests/170)
- [Pull Request #180: Use same default encoding for python2 and python3 (utf-8)](https://gitlab.com/tango-controls/pytango/-/merge_requests/180)

#### Bug fixes

- [Pull Request #178 (issue #177): Make CmdDoneEvent.argout writable](https://gitlab.com/tango-controls/pytango/-/merge_requests/178)
- [Pull Request #178: Add GIL control for ApiUtil.get_asynch_replies](https://gitlab.com/tango-controls/pytango/-/merge_requests/178)
- [Pull Request #187 (issue #186): Fix and extend client green mode](https://gitlab.com/tango-controls/pytango/-/merge_requests/187)

______________________________________________________________________

### 9.2.2

#### Features

- [Pull Request #104: Pipe Events](https://gitlab.com/tango-controls/pytango/-/merge_requests/104)
- [Pull Request #106: Implement pipe write (client and server, issue #9)](https://gitlab.com/tango-controls/pytango/-/merge_requests/106)
- [Pull Request #122: Dynamic commands](https://gitlab.com/tango-controls/pytango/-/merge_requests/122)
- [Pull Request #124: Add forward attribute](https://gitlab.com/tango-controls/pytango/-/merge_requests/124)
- [Pull Request #129: Implement mandatory property (issue #30)](https://gitlab.com/tango-controls/pytango/-/merge_requests/129)

#### Changes

- [Pull Request #109: Device Interface Change Events](https://gitlab.com/tango-controls/pytango/-/merge_requests/109)
- [Pull Request #113: Adding asyncio green mode documentation and a how-to on contributing](https://gitlab.com/tango-controls/pytango/-/merge_requests/113)
- [Pull Request #114: Added PEP8-ified files in tango module.](https://gitlab.com/tango-controls/pytango/-/merge_requests/114)
- [Pull Request #115: Commands polling tests (client and server)](https://gitlab.com/tango-controls/pytango/-/merge_requests/115)
- [Pull Request #116: Attribute polling tests (client and server)](https://gitlab.com/tango-controls/pytango/-/merge_requests/116)
- [Pull Request #117: Use official tango-controls conda channel](https://gitlab.com/tango-controls/pytango/-/merge_requests/117)
- [Pull Request #125: Forward attribute example](https://gitlab.com/tango-controls/pytango/-/merge_requests/125)
- [Pull Request #134: Linting pytango (with pylint + flake8)](https://gitlab.com/tango-controls/pytango/-/merge_requests/134)
- [Pull Request #137: Codacy badge in README and code quality policy in How to Contribute](https://gitlab.com/tango-controls/pytango/-/merge_requests/137)
- [Pull Request #143: Added missing PipeEventData & DevIntrChangeEventData](https://gitlab.com/tango-controls/pytango/-/merge_requests/143)

#### Bug fixes

- [Pull Request #85 (issue #84): Fix Gevent ThreadPool exceptions](https://gitlab.com/tango-controls/pytango/-/merge_requests/85)
- [Pull Request #94 (issue #93): Fix issues in setup file (GCC-7 build)](https://gitlab.com/tango-controls/pytango/-/merge_requests/94)
- [Pull Request #96: Filter badges from the long description](https://gitlab.com/tango-controls/pytango/-/merge_requests/96)
- [Pull Request #97: Fix/linker options](https://gitlab.com/tango-controls/pytango/-/merge_requests/97)
- [Pull Request #98: Refactor green mode for client and server APIs](https://gitlab.com/tango-controls/pytango/-/merge_requests/98)
- [Pull Request #101 (issue #100) check for None and return null string](https://gitlab.com/tango-controls/pytango/-/merge_requests/101)
- [Pull Request #102: Update server tests](https://gitlab.com/tango-controls/pytango/-/merge_requests/102)
- [Pull Request #103: Cache build objects to optimize travis builds](https://gitlab.com/tango-controls/pytango/-/merge_requests/103)
- [Pull Request #112 (issue #111): Use \_DeviceClass as tango device class constructor](https://gitlab.com/tango-controls/pytango/-/merge_requests/112)
- [Pull Request #128 (issue #127): Set default worker in server.py](https://gitlab.com/tango-controls/pytango/-/merge_requests/128)
- [Pull Request #135: Better exception handling in server.run and test context (issue #131)](https://gitlab.com/tango-controls/pytango/-/merge_requests/135)
- [Pull Request #142 (issue #142): Added missing PipeEventData & DevIntrChangeEventData](https://gitlab.com/tango-controls/pytango/-/merge_requests/143)
- [Pull Request #148 (issue #144): Expose utils helpers](https://gitlab.com/tango-controls/pytango/-/merge_requests/148)
- [Pull Request #149: Fix return value of proxy.subscribe_event](https://gitlab.com/tango-controls/pytango/-/merge_requests/149)
- [Pull Request #158 (issue #155): Fix timestamp and casing in utils.EventCallback](https://gitlab.com/tango-controls/pytango/-/merge_requests/158)

______________________________________________________________________

### 9.2.1

#### Features

- [Pull Requests #70: Add test_context and test_utils modules, used for pytango unit-testing](https://gitlab.com/tango-controls/pytango/-/issues/70)

#### Changes

- [Issue #51: Refactor platform specific code in setup file](https://gitlab.com/tango-controls/pytango/-/issues/51)
- [Issue #67: Comply with PEP 440 for pre-releases](https://gitlab.com/tango-controls/pytango/-/issues/67)
- [Pull Request #70: Add unit-testing for the server API](https://gitlab.com/tango-controls/pytango/-/issues/70)
- [Pull Request #70: Configure Travis CI for continuous integration](https://gitlab.com/tango-controls/pytango/-/issues/70)
- [Pull Request #76: Add unit-testing for the client API](https://gitlab.com/tango-controls/pytango/-/issues/76)
- [Pull Request #78: Update the python version classifiers](https://gitlab.com/tango-controls/pytango/-/issues/78)
- [Pull Request #80: Move tango object server to its own module](https://gitlab.com/tango-controls/pytango/-/issues/80)
- [Pull Request #90: The metaclass definition for tango devices is no longer mandatory](https://gitlab.com/tango-controls/pytango/-/issues/90)

#### Bug fixes

- [Issue #24: Fix dev_status dangling pointer bug](https://gitlab.com/tango-controls/pytango/-/issues/24)
- [Issue #57: Fix dev_state/status to be gevent safe](https://gitlab.com/tango-controls/pytango/-/issues/57)
- [Issue #58: Server gevent mode internal call hangs](https://gitlab.com/tango-controls/pytango/-/issues/58)
- [Pull Request #62: Several fixes in tango.databaseds](https://gitlab.com/tango-controls/pytango/-/issues/62)
- [Pull Request #63: Follow up on issue #21 (Fix Group.get_device method)](https://gitlab.com/tango-controls/pytango/-/issues/63)
- [Issue #64: Fix AttributeProxy.\_\_dev_proxy to be initialized with python internals](https://gitlab.com/tango-controls/pytango/-/issues/64)
- [Issue #74: Fix hanging with an asynchronous tango server fails to start](https://gitlab.com/tango-controls/pytango/-/issues/74)
- [Pull Request #81: Fix DeviceImpl documentation](https://gitlab.com/tango-controls/pytango/-/issues/81)
- [Issue #82: Fix attribute completion for device proxies with IPython >= 4](https://gitlab.com/tango-controls/pytango/-/issues/82)
- [Issue #84: Fix gevent threadpool exceptions](https://gitlab.com/tango-controls/pytango/-/issues/84)

______________________________________________________________________

### 9.2.0

#### Features

- [Issue #37: Add display_level and polling_period as optional arguments to command decorator](https://gitlab.com/tango-controls/pytango/-/issues/37)

#### Bug fixes

- Fix cache problem when using `DeviceProxy` through an `AttributeProxy`
- Fix compilation on several platforms
- [Issue #19: Defining new members in DeviceProxy has side effects](https://gitlab.com/tango-controls/pytango/-/issues/19)
- Fixed bug in `beacon.add_device`
- Fix for `get_device_list` if server_name is '\*'
- Fix `get_device_attribute_property2` if `prop_attr` is not `None`
- Accept `StdStringVector` in `put_device_property`
- Map 'int' to DevLong64 and 'uint' to DevULong64
- [Issue #22: Fix push_data_ready_event() deadlock](https://gitlab.com/tango-controls/pytango/-/issues/22)
- [Issue #28: Fix compilation error for constants.cpp](https://gitlab.com/tango-controls/pytango/-/issues/28)
- [Issue #21: Fix Group.get_device method](https://gitlab.com/tango-controls/pytango/-/issues/21)
- [Issue #33: Fix internal server documentation](https://gitlab.com/tango-controls/pytango/-/issues/33)

#### Changes

- Move ITango to another project
- Use `setuptools` instead of `distutils`
- Add `six` as a requirement
- Refactor directory structure
- Rename `PyTango` module to `tango` (`import PyTango` still works for backward compatibility)
- Add a ReST readme for GitHub and PyPI

ITango changes (moved to another project):
\- Fix itango event logger for python 3
\- Avoid deprecation warning with IPython 4.x
\- Use entry points instead of scripts

______________________________________________________________________

### 9.2.0a

#### Missing

- writtable pipes (client and server)
- dynamic commands (server)
- device interface change event (client and server)
- pipe event (client and server)

#### Bug fixes

- [776: [pytango][8.1.8] SyntaxError: invalid syntax](https://sourceforge.net/p/tango-cs/bugs/776/)

______________________________________________________________________

### 8.1.9

#### Features

- [PR #2: asyncio support for both client and server API](https://gitlab.com/tango-controls/pytango/-/merge_requests/2)
- [PR #6: Expose AutoTangoMonitor and AutoTangoAllowThreads](https://gitlab.com/tango-controls/pytango/-/merge_requests/6)

#### Bug fixes

- [PR #31: Get -l flags from pkg-config](https://gitlab.com/tango-controls/pytango/-/merge_requests/31)
- [PR #15: Rename itango script to itango3 for python3](https://gitlab.com/tango-controls/pytango/-/merge_requests/15)
- [PR #14: Avoid deprecation warning with IPython 4.x](https://gitlab.com/tango-controls/pytango/-/merge_requests/14)

______________________________________________________________________

### 8.1.8

#### Features

- [PR #3: Add a run_server class method to Device](https://gitlab.com/tango-controls/pytango/-/merge_requests/3)
- [PR #4: Add device inheritance](https://gitlab.com/tango-controls/pytango/-/merge_requests/4)
- [110: device property with auto update in database](https://sourceforge.net/p/tango-cs/feature-requests/110)

#### Bug fixes

- [690: Description attribute property](https://sourceforge.net/p/tango-cs/bugs/690/)
- [700: [pytango] useless files in the source distribution](https://sourceforge.net/p/tango-cs/bugs/700/)
- [701: Memory leak in command with list argument](https://sourceforge.net/p/tango-cs/bugs/701/)
- [704: Assertion failure when calling command with string array input type](https://sourceforge.net/p/tango-cs/bugs/704/)
- [705: Support boost_python lib name on Gentoo](https://sourceforge.net/p/tango-cs/bugs/705/)
- [714: Memory leak in PyTango for direct server command calls](https://sourceforge.net/p/tango-cs/bugs/714)
- [718: OverflowErrors with float types in 8.1.6](https://sourceforge.net/p/tango-cs/bugs/718/)
- [724: PyTango DeviceProxy.command_inout(\<str>) memory leaks](https://sourceforge.net/p/tango-cs/bugs/724/)
- [736: pytango FTBFS with python 3.4](https://sourceforge.net/p/tango-cs/bugs/736/)
- [747: PyTango event callback in gevent mode gets called in non main thread](https://sourceforge.net/p/tango-cs/bugs/736/)

______________________________________________________________________

### 8.1.6

#### Bug fixes

- [698: PyTango.Util discrepancy](https://sourceforge.net/p/tango-cs/bugs/698)
- [697: PyTango.server.run does not accept old Device style classes](https://sourceforge.net/p/tango-cs/bugs/697)

______________________________________________________________________

### 8.1.5

#### Bug fixes

- [687: [pytango] 8.1.4 unexpected files in the source package](https://sourceforge.net/p/tango-cs/bugs/687/)
- [688: PyTango 8.1.4 new style server commands don't work](https://sourceforge.net/p/tango-cs/bugs/688/)

______________________________________________________________________

### 8.1.4

#### Features

- [107: Nice to check Tango/PyTango version at runtime](https://sourceforge.net/p/tango-cs/feature-requests/107)

#### Bug fixes

- [659: segmentation fault when unsubscribing from events](https://sourceforge.net/p/tango-cs/bugs/659/)
- [664: problem while installing PyTango 8.1.1 with pip (using pip 1.4.1)](https://sourceforge.net/p/tango-cs/bugs/664/)
- [678: [pytango] 8.1.2 unexpected files in the source package](https://sourceforge.net/p/tango-cs/bugs/678/)
- [679: PyTango.server tries to import missing \_\_builtin\_\_ module on Python 3](https://sourceforge.net/p/tango-cs/bugs/679/)
- [680: Cannot import PyTango.server.run](https://sourceforge.net/p/tango-cs/bugs/680/)
- [686: Device property substitution for a multi-device server](https://sourceforge.net/p/tango-cs/bugs/686/)

______________________________________________________________________

### 8.1.3

*SKIPPED*

______________________________________________________________________

### 8.1.2

#### Features

- [98: PyTango.server.server_run needs additional post_init_callback parameter](https://sourceforge.net/p/tango-cs/feature-requests/98)
- [102: DevEncoded attribute should support a python buffer object](https://sourceforge.net/p/tango-cs/feature-requests/102)
- [103: Make creation of \*EventData objects possible in PyTango](https://sourceforge.net/p/tango-cs/feature-requests/103)

#### Bug fixes

- [641: python3 error handling issue](https://sourceforge.net/p/tango-cs/bugs/641/)
- [648: PyTango unicode method parameters fail](https://sourceforge.net/p/tango-cs/bugs/648/)
- [649: write_attribute of spectrum/image fails in PyTango without numpy](https://sourceforge.net/p/tango-cs/bugs/649/)
- [650: [pytango] 8.1.1 not compatible with ipyton 1.2.0-rc1](https://sourceforge.net/p/tango-cs/bugs/650/)
- [651: PyTango segmentation fault when run a DS that use attr_data.py](https://sourceforge.net/p/tango-cs/bugs/651/)
- [660: command_inout_asynch (polling mode) fails](https://sourceforge.net/p/tango-cs/bugs/660/)
- [666: PyTango shutdown sometimes blocks.](https://sourceforge.net/p/tango-cs/bugs/666/)

______________________________________________________________________

### 8.1.1

#### Features

- Implemented tango C++ 8.1 API

#### Bug fixes

- [527: set_value() for ULong64](https://sourceforge.net/p/tango-cs/bugs/527/)
- [573: [pytango] python3 error with unregistered device](https://sourceforge.net/p/tango-cs/bugs/573/)
- [611: URGENT fail to write attribute with PyTango 8.0.3](https://sourceforge.net/p/tango-cs/bugs/611/)
- [612: [pytango][8.0.3] failed to build from source on s390](https://sourceforge.net/p/tango-cs/bugs/612/)
- [615: Threading problem when setting a DevULong64 attribute](https://sourceforge.net/p/tango-cs/bugs/615/)
- [622: PyTango broken when running on Ubuntu 13](https://sourceforge.net/p/tango-cs/bugs/622/)
- [626: attribute_history extraction can raised an exception](https://sourceforge.net/p/tango-cs/bugs/626/)
- [628: Problem in installing PyTango 8.0.3 on Scientific Linux 6](https://sourceforge.net/p/tango-cs/bugs/628/)
- [635: Reading of ULong64 attributes does not work](https://sourceforge.net/p/tango-cs/bugs/635/)
- [636: PyTango log messages are not filtered by level](https://sourceforge.net/p/tango-cs/bugs/636/)
- [637: [pytango] segfault doing write_attribute on Group](https://sourceforge.net/p/tango-cs/bugs/637/)

______________________________________________________________________

### 8.1.0

*SKIPPED*

______________________________________________________________________

### 8.0.3

#### Features

- [88: Implement Util::server_set_event_loop method in python](https://sourceforge.net/p/tango-cs/feature-requests/88)

#### Bug fixes

- [3576353: [pytango] segfault on 'RestartServer'](https://sourceforge.net/tracker/?func=detail&aid=3576353&group_id=57612&atid=484769)
- [3579062: [pytango] Attribute missing methods](https://sourceforge.net/tracker/?func=detail&aid=3579062&group_id=57612&atid=484769)
- [3586337: [pytango] Some DeviceClass methods are not python safe](https://sourceforge.net/tracker/?func=detail&aid=3586337&group_id=57612&atid=484769)
- [3598514: DeviceProxy.\_\_setattr\_\_ break python's descriptors](https://sourceforge.net/tracker/?func=detail&aid=3598514&group_id=57612&atid=484769)
- [3607779: [pytango] IPython 0.10 error](https://sourceforge.net/tracker/?func=detail&aid=3607779&group_id=57612&atid=484769)
- [598: Import DLL by PyTango failed on windows](https://sourceforge.net/p/tango-cs/bugs/598/)
- [605: [pytango] use distutils.version module](https://sourceforge.net/p/tango-cs/bugs/605/)

______________________________________________________________________

### 8.0.2

#### Bug fixes

- [3570970: [pytango] problem during the python3 building](https://sourceforge.net/tracker/?func=detail&aid=3570970&group_id=57612&atid=484769)
- [3570971: [pytango] itango does not work without qtconsole](https://sourceforge.net/tracker/?func=detail&aid=3570971&group_id=57612&atid=484769)
- [3570972: [pytango] warning/error when building 8.0.0](https://sourceforge.net/tracker/?func=detail&aid=3570972&group_id=57612&atid=484769)
- [3570975: [pytango] problem during use of python3 version](https://sourceforge.net/tracker/?func=detail&aid=3570975&group_id=57612&atid=484769)
- [3574099: [pytango] compile error with gcc < 4.5](https://sourceforge.net/tracker/?func=detail&aid=3574099&group_id=57612&atid=484769)

______________________________________________________________________

### 8.0.1

*SKIPPED*

______________________________________________________________________

### 8.0.0

#### Features

- Implemented tango C++ 8.0 API
- Python 3k compatible

#### Bug fixes

- [3023857: DevEncoded write attribute not supported](https://sourceforge.net/tracker/?func=detail&aid=3023857&group_id=57612&atid=484769)
- [3521545: [pytango] problem with tango profile](https://sourceforge.net/tracker/?func=detail&aid=3521545&group_id=57612&atid=484769)
- [3530535: PyTango group writting fails](https://sourceforge.net/tracker/?func=detail&aid=3530535&group_id=57612&atid=484769)
- [3564959: EncodedAttribute.encode_xxx() methods don't accept bytearray](https://sourceforge.net/tracker/?func=detail&aid=3564959&group_id=57612&atid=484769)

______________________________________________________________________

### 7.2.4

#### Bug fixes

- [551: [pytango] Some DeviceClass methods are not python safe](https://sourceforge.net/p/tango-cs/bugs/551/)

______________________________________________________________________

### 7.2.3

#### Features

- [3495607: DeviceClass.device_name_factory is missing](https://sourceforge.net/tracker/?func=detail&aid=3495607&group_id=57612&atid=484772)

#### Bug fixes

- [3103588: documentation of PyTango.Attribute.Group](https://sourceforge.net/tracker/?func=detail&aid=3103588&group_id=57612&atid=484769)
- [3458336: Problem with pytango 7.2.2](https://sourceforge.net/tracker/?func=detail&aid=3458336&group_id=57612&atid=484769)
- [3463377: PyTango memory leak in read encoded attribute](https://sourceforge.net/tracker/?func=detail&aid=3463377&group_id=57612&atid=484769)
- [3487930: [pytango] wrong python dependency](https://sourceforge.net/tracker/?func=detail&aid=3487930&group_id=57612&atid=484769)
- [3511509: Attribute.set_value_date_quality for encoded does not work](https://sourceforge.net/tracker/?func=detail&aid=3511509&group_id=57612&atid=484769)
- [3514457: [pytango] TANGO_HOST multi-host support](https://sourceforge.net/tracker/?func=detail&aid=3514457&group_id=57612&atid=484769)
- [3520739: command_history(...) in PyTango](https://sourceforge.net/tracker/?func=detail&aid=3520739&group_id=57612&atid=484769)

______________________________________________________________________

### 7.2.2

#### Features

- [3305251: DS dynamic attributes discards some Attr properties](https://sourceforge.net/tracker/?func=detail&aid=3305251&group_id=57612&atid=484769)
- [3365792: DeviceProxy.\<cmd_name> could be documented](https://sourceforge.net/tracker/?func=detail&aid=3365792&group_id=57612&atid=484772)
- [3386079: add support for ipython 0.11](https://sourceforge.net/tracker/?func=detail&aid=3386079&group_id=57612&atid=484772)
- [3437654: throw python exception as tango exception](https://sourceforge.net/tracker/?func=detail&aid=3437654&group_id=57612&atid=484772)
- [3447477: spock profile installation](https://sourceforge.net/tracker/?func=detail&aid=3447477&group_id=57612&atid=484772)

#### Bug fixes

- [3372371: write attribute of DevEncoded doesn't work](https://sourceforge.net/tracker/?func=detail&aid=3372371&group_id=57612&atid=484769)
- [3374026: [pytango] pyflakes warning](https://sourceforge.net/tracker/?func=detail&aid=3374026&group_id=57612&atid=484769)
- [3404771: PyTango.MultiAttribute.get_attribute_list missing](https://sourceforge.net/tracker/?func=detail&aid=3404771&group_id=57612&atid=484769)
- [3405580: PyTango.MultiClassAttribute missing](https://sourceforge.net/tracker/?func=detail&aid=3405580&group_id=57612&atid=484769)

______________________________________________________________________

### 7.2.1

*SKIPPED*

______________________________________________________________________

### 7.2.0

#### Features

- [3286678: Add missing EncodedAttribute JPEG methods](https://sourceforge.net/tracker/?func=detail&aid=3286678&group_id=57612&atid=484772)

______________________________________________________________________

### 7.1.6

#### Bug fixes

- 7.1.5 distribution is missing some files

______________________________________________________________________

### 7.1.5

#### Bug fixes

- [3284174: 7.1.4 does not build with gcc 4.5 and tango 7.2.6](https://sourceforge.net/tracker/?func=detail&aid=3284174&group_id=57612&atid=484769)
- [3284265: [pytango][7.1.4] a few files without licence and copyright](https://sourceforge.net/tracker/?func=detail&aid=3284265&group_id=57612&atid=484769)
- [3284318: copyleft vs copyright](https://sourceforge.net/tracker/?func=detail&aid=3284318&group_id=57612&atid=484769)
- [3284434: [pytango][doc] few ERROR during the doc generation](https://sourceforge.net/tracker/?func=detail&aid=3284434&group_id=57612&atid=484769)
- [3284435: [pytango][doc] few warning during the doc generation](https://sourceforge.net/tracker/?func=detail&aid=3284435&group_id=57612&atid=484769)
- [3284440: [pytango][spock] the profile can't be installed](https://sourceforge.net/tracker/?func=detail&aid=3284440&group_id=57612&atid=484769)
- [3285185: PyTango Device Server does not load Class Properties values](https://sourceforge.net/tracker/?func=detail&aid=3285185&group_id=57612&atid=484769)
- [3286055: PyTango 7.1.x DS using Tango C++ 7.2.x seg faults on exit](https://sourceforge.net/tracker/?func=detail&aid=3286055&group_id=57612&atid=484769)

______________________________________________________________________

### 7.1.4

#### Features

- [3274309: Generic Callback for events](https://sourceforge.net/tracker/?func=detail&aid=3274309&group_id=57612&atid=484772)

#### Bug fixes

- [3011775: Seg Faults due to removed dynamic attributes](https://sourceforge.net/tracker/?func=detail&aid=3011775&group_id=57612&atid=484769)
- [3105169: PyTango 7.1.3 does not compile with Tango 7.2.X](https://sourceforge.net/tracker/?func=detail&aid=3105169&group_id=57612&atid=484769)
- [3107243: spock profile does not work with python 2.5](https://sourceforge.net/tracker/?func=detail&aid=3107243&group_id=57612&atid=484769)
- [3124427: PyTango.WAttribute.set_max_value() changes min value](https://sourceforge.net/tracker/?func=detail&aid=3124427&group_id=57612&atid=484769)
- [3170399: Missing documentation about is\_\<attr>\_allowed method](https://sourceforge.net/tracker/?func=detail&aid=3170399&group_id=57612&atid=484769)
- [3189082: Missing get_properties() for Attribute class](https://sourceforge.net/tracker/?func=detail&aid=3189082&group_id=57612&atid=484769)
- [3196068: delete_device() not called after server_admin.Kill()](https://sourceforge.net/tracker/?func=detail&aid=3196068&group_id=57612&atid=484769)
- [3257286: Binding crashes when reading a WRITE string attribute](https://sourceforge.net/tracker/?func=detail&aid=3257286&group_id=57612&atid=484769)
- [3267628: DP.read_attribute(, extract=List/tuple) write value is wrong](https://sourceforge.net/tracker/?func=detail&aid=3267628&group_id=57612&atid=484769)
- [3274262: Database.is_multi_tango_host missing](https://sourceforge.net/tracker/?func=detail&aid=3274262&group_id=57612&atid=484769)
- [3274319: EncodedAttribute is missing in PyTango (\<= 7.1.3)](https://sourceforge.net/tracker/?func=detail&aid=3274319&group_id=57612&atid=484769)
- [3277269: read_attribute(DevEncoded) is not numpy as expected](https://sourceforge.net/tracker/?func=detail&aid=3277269&group_id=57612&atid=484769)
- [3278946: DeviceAttribute copy constructor is not working](https://sourceforge.net/tracker/?func=detail&aid=3278946&group_id=57612&atid=484769)

#### Documentation

- Added {ref}`utilities` chapter
- Added {ref}`encoded` chapter
- Improved {ref}`server-new-api` chapter

______________________________________________________________________

### 7.1.3

#### Features

- tango logging with print statement
- tango logging with decorators
- from sourceforge:
- [3060380: ApiUtil should be exported to PyTango](https://sourceforge.net/tracker/?func=detail&aid=3060380&group_id=57612&atid=484772)

#### Bug fixes

- added licence header to all source code files
- spock didn't work without TANGO_HOST env. variable (it didn't recognize tangorc)
- spock should give a proper message if it tries to be initialized outside ipython
- [3048798: licence issue GPL != LGPL](https://sourceforge.net/tracker/?func=detail&aid=3048798&group_id=57612&atid=484769)
- [3073378: DeviceImpl.signal_handler raising exception crashes DS](https://sourceforge.net/tracker/?func=detail&aid=3073378&group_id=57612&atid=484769)
- [3088031: Python DS unable to read DevVarBooleanArray property](https://sourceforge.net/tracker/?func=detail&aid=3088031&group_id=57612&atid=484769)
- [3102776: PyTango 7.1.2 does not work with python 2.4 & boost 1.33.0](https://sourceforge.net/tracker/?func=detail&aid=3102776&group_id=57612&atid=484769)
- [3102778: Fix compilation warnings in linux](https://sourceforge.net/tracker/?func=detail&aid=3102778&group_id=57612&atid=484769)

______________________________________________________________________

### 7.1.2

#### Features

- [2995964: Dynamic device creation](https://sourceforge.net/tracker/?func=detail&aid=2995964&group_id=57612&atid=484772)
- [3010399: The DeviceClass.get_device_list that exists in C++ is missing](https://sourceforge.net/tracker/?func=detail&aid=3010399&group_id=57612&atid=484772)
- [3023686: Missing DeviceProxy.\<attribute name>](https://sourceforge.net/tracker/?func=detail&aid=3023686&group_id=57612&atid=484772)
- [3025396: DeviceImpl is missing some CORBA methods](https://sourceforge.net/tracker/?func=detail&aid=3025396&group_id=57612&atid=484772)
- [3032005: IPython extension for PyTango](https://sourceforge.net/tracker/?func=detail&aid=3032005&group_id=57612&atid=484772)
- [3033476: Make client objects pickable](https://sourceforge.net/tracker/?func=detail&aid=3033476&group_id=57612&atid=484772)
- [3039902: PyTango.Util.add_class would be useful](https://sourceforge.net/tracker/?func=detail&aid=3039902&group_id=57612&atid=484772)

#### Bug fixes

- [2975940: DS command with DevVarCharArray return type fails](https://sourceforge.net/tracker/?func=detail&aid=2975940&group_id=57612&atid=484769)
- [3000467: DeviceProxy.unlock is LOCKING instead of unlocking!](https://sourceforge.net/tracker/?func=detail&aid=3000467&group_id=57612&atid=484769)
- [3010395: Util.get_device\_\* methods don't work](https://sourceforge.net/tracker/?func=detail&aid=3010395&group_id=57612&atid=484769)
- [3010425: Database.dev_name does not work](https://sourceforge.net/tracker/?func=detail&aid=3010425&group_id=57612&atid=484769)
- [3016949: command_inout_asynch callback does not work](https://sourceforge.net/tracker/?func=detail&aid=3016949&group_id=57612&atid=484769)
- [3020300: PyTango does not compile with gcc 4.1.x](https://sourceforge.net/tracker/?func=detail&aid=3020300&group_id=57612&atid=484769)
- [3030399: Database put(delete)\_attribute_alias generates segfault](https://sourceforge.net/tracker/?func=detail&aid=3030399&group_id=57612&atid=484769)

______________________________________________________________________

### 7.1.1

#### Features

- Improved setup script
- Interfaced with PyPI
- Cleaned build script warnings due to unclean python C++ macro definitions
- [2985993: PyTango numpy command support](https://sourceforge.net/tracker/?func=detail&aid=2985993&group_id=57612&atid=484772)
- [2971217: PyTango.GroupAttrReplyList slicing](https://sourceforge.net/tracker/?func=detail&aid=2971217&group_id=57612&atid=484772)

#### Bug fixes

- [2983299: Database.put_property() deletes the property](https://sourceforge.net/tracker/?func=detail&aid=2983299&group_id=57612&atid=484769)
- [2953689: can not write_attribute scalar/spectrum/image](https://sourceforge.net/tracker/?func=detail&aid=2953689&group_id=57612&atid=484769)
- [2953030: PyTango doc installation](https://sourceforge.net/tracker/?func=detail&aid=2953030&group_id=57612&atid=484769)

______________________________________________________________________

### 7.1.0

#### Features

- [2908176: read\_\*, write\_\* and is\_\*\_allowed() methods can now be defined](https://sourceforge.net/tracker/?func=detail&aid=2908176&group_id=57612&atid=484772)
- [2941036: TimeVal conversion to time and datetime](https://sourceforge.net/tracker/?func=detail&aid=2941036&group_id=57612&atid=484772)
- added str representation on Attr, Attribute, DeviceImpl and DeviceClass

#### Bug fixes

- [2903755: get_device_properties() bug reading DevString properties](https://sourceforge.net/tracker/?func=detail&aid=2903755group_id=57612&atid=484769)
- [2909927: PyTango.Group.read_attribute() return values](https://sourceforge.net/tracker/?func=detail&aid=2909927&group_id=57612&atid=484769)
- [2914194: DevEncoded does not work](https://sourceforge.net/tracker/?func=detail&aid=2914194&group_id=57612&atid=484769)
- [2916397: PyTango.DeviceAttribute copy constructor does not work](https://sourceforge.net/tracker/?func=detail&aid=2916397&group_id=57612&atid=484769)
- [2936173: PyTango.Group.read_attributes() fails](https://sourceforge.net/tracker/?func=detail&aid=2936173&group_id=57612&atid=484769)
- [2949099: Missing PyTango.Except.print_error_stack](https://sourceforge.net/tracker/?func=detail&aid=2949099&group_id=57612&atid=484769)

______________________________________________________________________

### 7.1.0rc1

#### Features

- v = image_attribute.get_write_value() returns square sequences (arrays of
  arrays, or numpy objects) now instead of flat lists. Also for spectrum
  attributes a numpy is returned by default now instead.
- image_attribute.set_value(v) accepts numpy arrays now or square sequences
  instead of just flat lists. So, dim_x and dim_y are useless now. Also the
  numpy path is faster.
- new enum AttrSerialModel
- Attribute new methods: set(get)\_attr_serial_model, set_change_event,
  set_archive_event, is_change_event, is_check_change_event,
  is_archive_criteria, is_check_archive_criteria, remove_configuration
- added support for numpy scalars in tango operations like write_attribute
  (ex: now a DEV_LONG attribute can receive a numpy.int32 argument in a
  write_attribute method call)

#### Bug fixes

- DeviceImpl.set_value for scalar attributes
- DeviceImpl.push\_\*\*\*\_event
- server commands with DevVar\*\*\*StringArray as parameter or as return type
- in windows,a bug in PyTango.Util prevented servers from starting up
- DeviceImpl.get_device_properties for string properties assigns only first
  character of string to object member instead of entire string
- added missing methods to Util
- exported SubDevDiag class
- error in read/events of attributes of type DevBoolean READ_WRITE
- error in automatic unsubscribe events of DeviceProxy when the object
  disapears (happens only on some compilers with some optimization flags)
- fix possible bug when comparing attribute names in DeviceProxy
- pretty print of DevFailed -> fix deprecation warning in python 2.6
- device class properties where not properly fetched when there is no
  property value defined
- memory leak when converting DevFailed exceptions from C++ to python
- python device server file without extension does not start

#### Documentation

- Improved FAQ
- Improved compilation chapter
- Improved migration information
