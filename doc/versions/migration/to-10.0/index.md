(pytango-migration-guide-9-5-10-0)=

# Moving to v10.0

This chapter describes how to migrate to
PyTango versions 10.0.x from 9.5.x and earlier.

```{toctree}
:maxdepth: 3

deps-install
asyncio
quality-event
```
