# Device

```{eval-rst}
.. currentmodule:: tango

```

## DeviceImpl

```{eval-rst}
.. autoclass:: tango.LatestDeviceImpl
    :members:
    :inherited-members:
```
