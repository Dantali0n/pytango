# Util

```{eval-rst}
.. currentmodule:: tango
```

```{eval-rst}
.. autoclass:: tango.Util
    :members:
    :inherited-members:
```
