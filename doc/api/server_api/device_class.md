# DeviceClass

```{eval-rst}
.. currentmodule:: tango
```

```{eval-rst}
.. autoclass:: tango.DeviceClass
    :members:
```
