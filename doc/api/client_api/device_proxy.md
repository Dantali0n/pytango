```{eval-rst}
.. currentmodule:: tango
```

# DeviceProxy

```{eval-rst}
.. autoclass:: tango.DeviceProxy
    :show-inheritance:
    :members:
    :inherited-members:
```

```{eval-rst}
.. autofunction:: tango.get_device_proxy
```
