```{eval-rst}
.. currentmodule:: tango
```

# Enumerations & other classes

## Enumerations

```{eval-rst}
.. autoclass:: tango.LockerLanguage
```

```{eval-rst}
.. autoclass:: tango.CmdArgType
```

```{eval-rst}
.. autoclass:: tango.MessBoxType
```

```{eval-rst}
.. autoclass:: tango.PollObjType
```

```{eval-rst}
.. autoclass:: tango.PollCmdCode
```

```{eval-rst}
.. autoclass:: tango..SerialModel
```

```{eval-rst}
.. autoclass:: tango.AttReqType
```

```{eval-rst}
.. autoclass:: tango.LockCmdCode
```

```{eval-rst}
.. autoclass:: tango.LogLevel
```

```{eval-rst}
.. autoclass:: tango.LogTarget
```

```{eval-rst}
.. autoclass:: tango.EventType
```

```{eval-rst}
.. autoclass:: tango.KeepAliveCmdCode
```

```{eval-rst}
.. autoclass:: tango.AccessControlType
```

```{eval-rst}
.. autoclass:: tango.asyn_req_type
```

```{eval-rst}
.. autoclass:: tango.cb_sub_model
```

```{eval-rst}
.. autoclass:: tango.AttrQuality
```

```{eval-rst}
.. autoclass:: tango.AttrWriteType
```

```{eval-rst}
.. autoclass:: tango.AttrDataFormat
```

```{eval-rst}
.. autoclass:: tango.PipeWriteType
```

```{eval-rst}
.. autoclass:: tango.DevSource
```

```{eval-rst}
.. autoclass:: tango.ErrSeverity
```

```{eval-rst}
.. autoclass:: tango.DevState
```

```{eval-rst}
.. autoclass:: tango.DispLevel
```

```{eval-rst}
.. autoclass:: tango.GreenMode

```

## Other classes

```{eval-rst}
.. autoclass:: tango.Release
    :members:
```

```{eval-rst}
.. autoclass:: tango.TimeVal
    :members:
```
