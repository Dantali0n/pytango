% pytango-client-green-api:

# Green API

Summary:
: - {func}`tango.get_green_mode`
  - {func}`tango.set_green_mode`
  - {func}`tango.asyncio.DeviceProxy`
  - {func}`tango.futures.DeviceProxy`
  - {func}`tango.gevent.DeviceProxy`

```{eval-rst}
.. autofunction:: tango.get_green_mode
```

```{eval-rst}
.. autofunction:: tango.set_green_mode
```

```{eval-rst}
.. autofunction:: tango.asyncio.DeviceProxy
```

```{eval-rst}
.. autofunction:: tango.futures.DeviceProxy
```

```{eval-rst}
.. autofunction:: tango.gevent.DeviceProxy
```
