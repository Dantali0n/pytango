```{eval-rst}
.. currentmodule:: tango
```

(advanced)=

# How-to guides

In this section, we provide information, useful for advanced PyTango developers.

```{toctree}
:maxdepth: 2

how-to/testing
how-to/multiprocessing
how-to/starting_device
how-to/server_old_api
how-to/telemetry
how-to/databaseds
how-to/how-to-contribute
```
