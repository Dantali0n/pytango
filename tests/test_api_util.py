import os

from tango.api_util import ApiUtil


def test_get_env_var():

    key = "MYTEST"
    value = "abcd"

    os.environ[key] = value
    assert ApiUtil.get_env_var(key) == value
