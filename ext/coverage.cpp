/******************************************************************************
  This file is part of PyTango (http://pytango.rtfd.io)

  Copyright 2006-2012 CELLS / ALBA Synchrotron, Bellaterra, Spain
  Copyright 2013-2014 European Synchrotron Radiation Facility, Grenoble, France

  Distributed under the terms of the GNU Lesser General Public License,
  either version 3 of the License, or (at your option) any later version.
  See LICENSE.txt for more info.
******************************************************************************/

#include "precompiled_header.hpp"

#include <boost/python.hpp>

#ifdef PYTANGO_ENABLE_COVERAGE

extern "C" void __gcov_dump();

void dump_cpp_coverage()
{
    __gcov_dump();
}

#else

void dump_cpp_coverage()
{
    throw std::runtime_error("No coverage support enabled, pass \"-DPYTANGO_ENABLE_COVERAGE=True\" to cmake "
                             "and recompile to enable it.");
}

#endif

void export_coverage_helper()
{
    bopy::def("_dump_cpp_coverage", &dump_cpp_coverage);
}
